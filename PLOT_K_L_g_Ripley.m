%   PLOT_K_L_g_Ripley.m
%   Anders Sejr Hansen
clear; clc; close all;
%   DESCRIPTION
%   Take the output of K-Ripley R-code and average several cells and plot
%   the key output

DataSet = 1;

% Define the dataset
if DataSet == 1
    r = 1:1300;
    DataStruc = struct;
    DataStruc(1,1).GenPath = '/Users/anderssejrhansen/Documents/20180711_C59_D2_PALM_PA646/Test2/';
    DataStruc(1,1).D2_DirList = {'20180711_mESC_C59_D2_e10-3xHA-Halo-mCTCF_500nM_PA-JF646_40-500mW-633_rising-405_15x_25msCam_cell01Nucleus1',...
                                 '20180711_mESC_C59_D2_e10-3xHA-Halo-mCTCF_500nM_PA-JF646_40-500mW-633_rising-405_15x_25msCam_cell02Nucleus1'};
    DataStruc(1,1).D2_Include = [1,2];
    DataStruc(1,1).C59_DirList = {'20180711_mESC_C59_Halo-mCTCF_500nM_PA-JF646_40-500mW-633_rising-405_15x_25msCam_cell01Nucleus1',...
                                  '20180711_mESC_C59_Halo-mCTCF_500nM_PA-JF646_40-500mW-633_rising-405_15x_25msCam_cell01Nucleus2'};
    DataStruc(1,1).C59_Include = [1,2];
    D2_count = 0;
    C59_count = 0; 
end

% create cell arrays for storing the data
L_func_D2 = cell(1,1); L_func_C59 = cell(1,1);
K_func_D2 = cell(1,1); K_func_C59 = cell(1,1);
g_func_D2 = cell(1,1); g_func_C59 = cell(1,1);
name_D2 = cell(1,1); name_C59 = cell(1,1);

% load in the data:
for DataIter = 1:length(DataStruc)
    % go through the D2:
    for iter = 1:length(DataStruc(DataIter,1).D2_DirList)
        if DataStruc(DataIter,1).D2_Include(iter) == iter
            % load in the MAT file
            temp = load([DataStruc(DataIter,1).GenPath, filesep, DataStruc(DataIter,1).D2_DirList{iter}, filesep, 'KRipleyScripts', filesep, 'res_ripley_1_.mat']);
            D2_count = D2_count + 1; % increment the counter
            L_func_D2{D2_count,1} = temp.l_func; % store the data
            K_func_D2{D2_count,1} = temp.kripley; % store the data
            g_func_D2{D2_count,1} = temp.g_func; % store the data
            name_D2{D2_count,1} = DataStruc(DataIter,1).D2_DirList{iter};
            disp(num2str(DataIter));
        end
    end
    % go through the C59:
    for iter = 1:length(DataStruc(DataIter,1).C59_DirList)
        if DataStruc(DataIter,1).C59_Include(iter) == iter
            % load in the MAT file
            temp = load([DataStruc(DataIter,1).GenPath, filesep, DataStruc(DataIter,1).C59_DirList{iter}, filesep, 'KRipleyScripts', filesep, 'res_ripley_1_.mat']);
            C59_count = C59_count + 1; % increment the counter
            L_func_C59{C59_count,1} = temp.l_func; % store the data
            K_func_C59{C59_count,1} = temp.kripley; % store the data
            g_func_C59{C59_count,1} = temp.g_func; % store the data
            name_C59{C59_count,1} = DataStruc(DataIter,1).C59_DirList{iter};
        end
    end 
end

% collect the means:
L_func_D2_all = zeros(D2_count, length(r)); L_func_C59_all = zeros(C59_count, length(r));
K_func_D2_all = zeros(D2_count, length(r)); K_func_C59_all = zeros(C59_count, length(r));
g_func_D2_all = zeros(D2_count, length(r)); g_func_C59_all = zeros(C59_count, length(r));
for iter = 1:D2_count
    L_func_D2_all(iter,:) = L_func_D2{iter};
    K_func_D2_all(iter,:) = K_func_D2{iter};
    g_func_D2_all(iter,:) = g_func_D2{iter};
end
for iter = 1:C59_count
    L_func_C59_all(iter,:) = L_func_C59{iter};
    K_func_C59_all(iter,:) = K_func_C59{iter};
    g_func_C59_all(iter,:) = g_func_C59{iter};
end

% Plot single-cell figures:
RowCols = ceil(sqrt(D2_count + C59_count));
figure2 = figure('position',[100 100 1200 1200]); %[x y width height]
PlotCount = 0;
% make the plots
for iter = 1:D2_count
    PlotCount = PlotCount + 1; % increment the counter
    subplot(RowCols, RowCols,PlotCount); hold on;
    plot(r, L_func_D2{iter}, '-', 'LineWidth', 2, 'Color', 'k');
    xlim([0 max(r)]);
    xlabel('r (nm)', 'FontSize',9, 'FontName', 'Helvetica');
    ylabel('L(r)-r', 'FontSize',9, 'FontName', 'Helvetica');
    title(['L(r)-r for D2: ', name_D2{iter}(end-26:end)], 'FontSize',8, 'FontName', 'Helvetica', 'FontWeight', 'Normal', 'Interpreter', 'none');
    hold off;
end
% make the plots
for iter = 1:C59_count
    PlotCount = PlotCount + 1; % increment the counter
    subplot(RowCols, RowCols,PlotCount); hold on;
    plot(r, L_func_C59{iter}, '-', 'LineWidth', 2, 'Color', [237/255, 28/255, 36/255]);
    xlim([0 max(r)]);
    xlabel('r (nm)', 'FontSize',9, 'FontName', 'Helvetica');
    ylabel('L(r)-r', 'FontSize',9, 'FontName', 'Helvetica');
    title(['L(r)-r for C59: ', name_C59{iter}(end-26:end)], 'FontSize',8, 'FontName', 'Helvetica', 'FontWeight', 'Normal', 'Interpreter', 'none');
    hold off;
end

% Generate Ripley CSR:
Ripley_CSR = pi .* r.^2;

% Plot the averages:
figure1 = figure('position',[200 200 800 800]); %[x y width height]
% PLOT the L(r)-r:
subplot(2,2,1); hold on;
mean_L_D2 = mean(L_func_D2_all,1); stderr_L_D2 = std(L_func_D2_all,1)/sqrt(D2_count);
errorbar(r(1:10:end), mean_L_D2(1:10:end), stderr_L_D2(1:10:end), 'ko', 'MarkerFaceColor', 'k', 'MarkerSize', 4);
mean_L_C59 = mean(L_func_C59_all,1); stderr_L_C59 = std(L_func_C59_all,1)/sqrt(C59_count);
errorbar(r(1:10:end), mean_L_C59(1:10:end), stderr_L_C59(1:10:end), 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerFaceColor', [237/255, 28/255, 36/255], 'MarkerSize', 4);
xlim([0 max(r)]);
xlabel('r (nm)', 'FontSize',9, 'FontName', 'Helvetica');
ylabel('L(r)-r', 'FontSize',9, 'FontName', 'Helvetica');
title('L(r)-r for C59 vs. D2', 'FontSize',11, 'FontName', 'Helvetica', 'FontWeight', 'Normal', 'Interpreter', 'none');
legend(['D2:', num2str(D2_count)], ['C59:', num2str(C59_count)], 'Location', 'NorthWest'); legend boxoff
hold off;

% PLOT the K-ripley
subplot(2,2,2); hold on;
mean_K_D2 = mean(K_func_D2_all,1); stderr_K_D2 = std(K_func_D2_all,1)/sqrt(D2_count);
errorbar(r(1:10:end), mean_K_D2(1:10:end), stderr_K_D2(1:10:end), 'ko', 'MarkerFaceColor', 'k', 'MarkerSize', 4);
mean_K_C59 = mean(K_func_C59_all,1); stderr_K_C59 = std(K_func_C59_all,1)/sqrt(C59_count);
errorbar(r(1:10:end), mean_K_C59(1:10:end), stderr_K_C59(1:10:end), 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerFaceColor', [237/255, 28/255, 36/255], 'MarkerSize', 4);
plot(r, Ripley_CSR, 'k--', 'LineWidth', 1);
xlim([9 max(r)]);
ylim([min([mean_K_D2(9) mean_K_C59(9)]) 1.3*max([max(mean_K_D2) max(mean_K_C59) ])]);
xlabel('r (nm)', 'FontSize',9, 'FontName', 'Helvetica');
ylabel('K-Ripley K(r)', 'FontSize',9, 'FontName', 'Helvetica');
title('K-Ripley analysis for C59 vs. D2', 'FontSize',11, 'FontName', 'Helvetica', 'FontWeight', 'Normal', 'Interpreter', 'none');
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
legend('D2', 'C59', 'CSR', 'Location', 'NorthWest'); legend boxoff
hold off;

% PLOT the Pair-Correlation Function
subplot(2,2,3); hold on;
mean_g_D2 = mean(g_func_D2_all,1); stderr_g_D2 = std(g_func_D2_all,1)/sqrt(D2_count);
errorbar(r(1:10:end), mean_g_D2(1:10:end), stderr_g_D2(1:10:end), 'ko', 'MarkerFaceColor', 'k', 'MarkerSize', 4);
mean_g_C59 = mean(g_func_C59_all,1); stderr_g_C59 = std(g_func_C59_all,1)/sqrt(C59_count);
errorbar(r(1:10:end), mean_g_C59(1:10:end), stderr_g_C59(1:10:end), 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerFaceColor', [237/255, 28/255, 36/255], 'MarkerSize', 4);
xlim([0 max(r)]);
xlabel('r (nm)', 'FontSize',9, 'FontName', 'Helvetica');
ylabel('pair correlation, g(r)', 'FontSize',9, 'FontName', 'Helvetica');
title('g(r) for C59 vs. D2', 'FontSize',11, 'FontName', 'Helvetica', 'FontWeight', 'Normal', 'Interpreter', 'none');
legend('D2', 'C59', 'Location', 'NorthEast'); legend boxoff
hold off;

% PLOT the Pair-Correlation Function: ZOOM
subplot(2,2,4); hold on;
mean_g_D2 = mean(g_func_D2_all,1); stderr_g_D2 = std(g_func_D2_all,1)/sqrt(D2_count);
errorbar(r(1:10:end), mean_g_D2(1:10:end), stderr_g_D2(1:10:end), 'ko', 'MarkerFaceColor', 'k', 'MarkerSize', 4);
mean_g_C59 = mean(g_func_C59_all,1); stderr_g_C59 = std(g_func_C59_all,1)/sqrt(C59_count);
errorbar(r(1:10:end), mean_g_C59(1:10:end), stderr_g_C59(1:10:end), 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerFaceColor', [237/255, 28/255, 36/255], 'MarkerSize', 4);
xlim([0 255]);
xlabel('r (nm)', 'FontSize',9, 'FontName', 'Helvetica');
ylabel('pair correlation, g(r)', 'FontSize',9, 'FontName', 'Helvetica');
title('g(r) for C59 vs. D2: Zoom-in', 'FontSize',11, 'FontName', 'Helvetica', 'FontWeight', 'Normal', 'Interpreter', 'none');
legend('D2', 'C59', 'Location', 'NorthEast'); legend boxoff
hold off;

%%%%% SAVE A PDF FIGURE:
set(figure1,'Units','Inches');
pos = get(figure1,'Position');
set(figure1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(figure1,[DataStruc(1,1).GenPath, filesep, 'SummaryOfK-RipleyAnalysis.pdf'],'-dpdf','-r0');