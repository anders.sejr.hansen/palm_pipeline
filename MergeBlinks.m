function [ PALM_Locs, TrackedLocs ] = MergeBlinks( ParamStruct, BaSDI_matrix )
%MergeBlinks Merge multiple appearances into 1 localization
%   Anders Sejr Hansen, August 2018
%   Take raw, drift-corrected localizations from a PALM movie and the merge
%   multiple appearance of the same molecule into 1 localization using
%   NearestNeighbour tracking
tic;

%   Please note that this function uses a custom-modified version of
%   Jean-Yves Tinevez's SimpleTracker. The modified version is called "simpletracker_ASH"
%   and the output format is different and also fixed a bug where the code
%   could not handle empty frames. 

% For NearestNeighbor tracking we need to generate a cell array with the
% following properties:
%   - cell array should have N columns equal to the number of frames
%   - each cell array element should be a column vector with XY coordinates
PointsCell = cell(max(BaSDI_matrix(:,3)), 1); 
for iter = 1:size(BaSDI_matrix)
    CurrFrame = BaSDI_matrix(iter,3);
    PointsCell{CurrFrame} = vertcat(PointsCell{CurrFrame}, BaSDI_matrix(iter,1:2));
end

disp(['doing Nearest Neighbor merging of multiple appearances for ', num2str(size(BaSDI_matrix,1)), ' localizations... Hold on tight']);
% Merge multiple appearance using a modified SimplerTracker_ASH
TrackedLocs = simpletracker_ASH(PointsCell, 'MaxLinkingDistance', ParamStruct.MaxLinkingDistance,...
                                'MaxGapClosing', ParamStruct.MaxGapClosing, 'Method', 'NearestNeighbor');
% Note the following:
%   TrackedLocs is a cell array with length N, where N is the number of
%   trajectories
%   Each "trajectory" is a Nx3 matrix, where:
%       - N rows corresponding to the number of localizations
%       - column 1: X, column 2: Y, column 3: column


% Now that we have done the tracking, we need to merge trajectories into a
% single localization. When you have multiple appearances it's a bit
% ambigious how to assign the frame number. E.g. if the molecule was there
% in frames 1,2,3,4, which frame should we assign the merged localization?
% Here we will use the mean, rounded to an integer. 
% Store all merged localizations in PALM_Locs, where
%   - column 1: X-coordinate in nm
%   - column 2: Y-coordinate in nm
%   - column 3: mean frame, rounded
PALM_Locs = zeros(length(TrackedLocs),3);
for iter = 1:size(PALM_Locs,1)
    curr_Loc = TrackedLocs{iter};
    curr_X = mean(curr_Loc(:,1));
    curr_Y = mean(curr_Loc(:,2));
    curr_Frame = round(mean(curr_Loc(:,3)));
    PALM_Locs(iter,:) = [curr_X, curr_Y, curr_Frame];
end

% OK, all done
toc;
end

