function [ BaSDI_matrix, DriftStruct ] = IterativeBaSDI_DriftCorrect( ParamStruct, MatFile )
% ITERATIVEBASDI_DRIFTCORRECT     Do drift correction
%   Anders Sejr Hansen, August 2018
%   Iteratively drift correct using BaSDI-algorithm, which is a
%   fiducial-marker free approach to correcting for XY-drift during PALM
%   experiments
tic;
%   Un-pack the key parameters
FramesBin = ParamStruct.FramesBin;   %time window, frames, to average over
PixelBin = ParamStruct.PixelBin;      % pixel size in nanometers for BaSDI binning
Iterations = ParamStruct.Iterations;     % Number of BaSDI iterations

%   Load in the PALM file in the TrackedPar format
load(MatFile);

% Generate a DriftStruct for storing the drift for each iteration and the
% total drift
DriftStruct = struct;
DriftStruct.IterDrift_XY_nm = cell(Iterations,1);

% Also need a temp BaSDI_matrix struct:
BaSDI_matrix_struct = struct; % this is temporary, since there is no point in keeping the partially drift-corrected localizations
for iter=1:Iterations
    BaSDI_matrix_struct(iter,1).BaSDI_matrix_corr = [];
end

% convert all trackedPar structured arrays into a gigantic matrix.
% To pre-initialize memory, first guess the matrix size:
BaSDI_matrix = zeros(length(trackedPar)*10, 3); % col 1,2,3: x, y, Frame
CurrRow=1;
for iter = 1:length(trackedPar)
    NewRow = CurrRow+length(trackedPar(iter).Frame)-1;
    BaSDI_matrix(CurrRow:NewRow,1:3) = horzcat(trackedPar(iter).xy, trackedPar(iter).Frame);
    CurrRow = NewRow+1;
end
% now see if you need to remove padding from the matrix:
if CurrRow <= size(BaSDI_matrix,1)
    BaSDI_matrix(CurrRow:end,:) = [];
end
% find the max frame for binning:
LastFrame = max(BaSDI_matrix(:,3));


% iteration 1:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% Run BaSDI on uncorrected data %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convert to cell array:
% BaSDI is written in a way where it cannot handle having e.g. 49777
% frames. So need to round to the nearest FramesBin:
max_frame = FramesBin*ceil(LastFrame/FramesBin);
BaSDI_cell_array = cell(1,max_frame);
for iter = 1:size(BaSDI_matrix,1)
    curr_frame = BaSDI_matrix(iter,3);
    BaSDI_cell_array{curr_frame} = vertcat(BaSDI_cell_array{curr_frame}, [BaSDI_matrix(iter,1), BaSDI_matrix(iter,2)]);
end

% concatenate cell array
BaSDI_cell_array_small = cat_cellarray(BaSDI_cell_array, FramesBin); % bin frames together to speed things up

% run BaSDI:
S = BaSDI(BaSDI_cell_array_small, PixelBin); % this is the key step
disp('BaSDI iteration 1 finished...');

% get drift
[Drift_XY_vals, ~] = processing_result(S.g); % get drift (use ~ since you don't need "Drift_sigma")

% BaSDI gives the drift in units of the Pixel Bin, and only one value for
% each FrameBin. So need to do some linear interpolation to get drift for
% each frame. 
% linear interpolate to get the drift at each frame; convert from pixel units to nanometers
Drift_XY_nm = horzcat(PixelBin .* interp1(vertcat(1, (501:FramesBin:(LastFrame+FramesBin))'), vertcat(0, [Drift_XY_vals(:,1); Drift_XY_vals(end,1)]), (1:(LastFrame+FramesBin))'),...
                            PixelBin .* interp1(vertcat(1, (501:FramesBin:(LastFrame+FramesBin))'), vertcat(0, [Drift_XY_vals(:,2); Drift_XY_vals(end,2)]), (1:(LastFrame+FramesBin))'));

% Now correct the localizations by subtracting the drift:
BaSDI_matrix_corr = zeros(size(BaSDI_matrix,1), size(BaSDI_matrix,2));
BaSDI_matrix_corr(:,3) = BaSDI_matrix(:,3);
for iter=1:size(BaSDI_matrix_corr,1)
    % correct x drift:
    BaSDI_matrix_corr(iter,1) = BaSDI_matrix(iter,1) - Drift_XY_nm(BaSDI_matrix(iter,3),1);
    % correct y drift:
    BaSDI_matrix_corr(iter,2) = BaSDI_matrix(iter,2) - Drift_XY_nm(BaSDI_matrix(iter,3),2);
end
% save this to the structured array:
BaSDI_matrix_struct(1,1).BaSDI_matrix_corr = BaSDI_matrix_corr;
DriftStruct.IterDrift_XY_nm{1} = Drift_XY_nm; 

% now clear out all the used stuff:
clear BaSDI_matrix_corr BaSDI_cell_array_small BaSDI_cell_array Drift_XY_vals Drift_XY_nm

% now proceed to subsequent iterations:
if Iterations > 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%% Run BaSDI on corrected data %%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for BaSDIiter = 2:Iterations
        
        % Get out the prior iterations BaSDI matrix:
        prior_BaSDI_matrix = BaSDI_matrix_struct(BaSDIiter-1,1).BaSDI_matrix_corr;
        
        % Construct new cell array:
        BaSDI_cell_array = cell(1,max_frame);
        for iter = 1:size(prior_BaSDI_matrix,1)
            curr_frame = prior_BaSDI_matrix(iter,3);
            BaSDI_cell_array{curr_frame} = vertcat(BaSDI_cell_array{curr_frame}, [prior_BaSDI_matrix(iter,1), prior_BaSDI_matrix(iter,2)]);
        end
        
        % concatenate cell array
        BaSDI_cell_array_small = cat_cellarray(BaSDI_cell_array, FramesBin); % bin frames together to speed things up

        % run BaSDI:
        S = BaSDI(BaSDI_cell_array_small, PixelBin); % this is the key step
        disp(['BaSDI iteration ', num2str(BaSDIiter), ' finished...']);
        
        % get drift
        [Drift_XY_vals, ~] = processing_result(S.g); % get drift (use ~ since you don't need "Drift_sigma")
        
        % linear interpolate to get the drift at each frame; convert from pixel units to nanometers
        Drift_XY_nm = horzcat(PixelBin .* interp1(vertcat(1, (501:FramesBin:(LastFrame+FramesBin))'), vertcat(0, [Drift_XY_vals(:,1); Drift_XY_vals(end,1)]), (1:(LastFrame+FramesBin))'),...
                                    PixelBin .* interp1(vertcat(1, (501:FramesBin:(LastFrame+FramesBin))'), vertcat(0, [Drift_XY_vals(:,2); Drift_XY_vals(end,2)]), (1:(LastFrame+FramesBin))'));

        % Now correct the localizations by subtracting the drift:
        BaSDI_matrix_corr = zeros(size(prior_BaSDI_matrix,1), size(prior_BaSDI_matrix,2));
        BaSDI_matrix_corr(:,3) = prior_BaSDI_matrix(:,3);
        for iter=1:size(BaSDI_matrix_corr,1)
            % correct x drift:
            BaSDI_matrix_corr(iter,1) = prior_BaSDI_matrix(iter,1) - Drift_XY_nm(prior_BaSDI_matrix(iter,3),1);
            % correct y drift:
            BaSDI_matrix_corr(iter,2) = prior_BaSDI_matrix(iter,2) - Drift_XY_nm(prior_BaSDI_matrix(iter,3),2);
        end
        % save this to the structured array:
        BaSDI_matrix_struct(BaSDIiter,1).BaSDI_matrix_corr = BaSDI_matrix_corr;
        DriftStruct.IterDrift_XY_nm{BaSDIiter,1} = Drift_XY_nm; 
        
        
        % now clear out all the used stuff:
        clear BaSDI_matrix_corr BaSDI_cell_array_small BaSDI_cell_array Drift_XY_vals Drift_XY_nm BaSDI_matrix
    end
end

% OK, now you finished all of the drift-correction iterations. So now you
% need to collect the final info that you need:
BaSDI_matrix = BaSDI_matrix_struct(end,1).BaSDI_matrix_corr; % the final most-drift corrected BaSDI_matrix

% calculate the total drift:
TotalDrift = zeros(size(DriftStruct.IterDrift_XY_nm{1},1), size(DriftStruct.IterDrift_XY_nm{1},2));
for iter = 1:Iterations
    % total drift is the sum of the drift from each iteration:
    TotalDrift = TotalDrift + DriftStruct.IterDrift_XY_nm{iter};
end
DriftStruct.TotalDrift = TotalDrift; % save to the drift structure
toc;
end

