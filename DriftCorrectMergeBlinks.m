%   DriftCorrectMergeBlinks.m
%   Anders Sejr Hansen, July 2018
clear; clc; close all;

%   DESCRIPTION
%   This is a new 2018 version of the PALM pipeline, which is a bit
%   different from the 2016 pipeline. At the experimental side, the movies
%   are bigger and generally contain 2-3 nuclei. This is to make the
%   Bayesian BaSDI based drift correction more robust
%   At the computational side, this script will do 2 key steps: 
%       1. BaSDI-based drift correction
%       2. Particles are merged to remove blinkers using a custom-modified version of Jean-Yves
%       Tineviz's nearest-neighbour simple tracker
%
%   The data is in the trackedPar format, so first, you need to "untrack"
%   the data and convert to the format that RCC likes.
%   Note the BaSDI wants to work in pixel-units

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.   
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
% 
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you 
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%add Path for functions:
addpath('BaSDI-master_ASH'); addpath('SimpleTracker_ASH');
% INSERT THE FILEPATH TO YOUR INPUT DATA AND THE PATH FOR THE OUTPUT HERE:
InputPath = '/Users/anderssejrhansen/Dropbox/MatLab/Lab/Microscopy/SingleParticleTracking/PALM/PALM_reconstruction_data/20180907_C59_D2_PA_JF549_PALM/';
OutputPath = '/Users/anderssejrhansen/Dropbox/MatLab/Lab/Microscopy/SingleParticleTracking/PALM/PALM_reconstruction_data/20180907_C59_D2_PA_JF549_PALM/Analyzed/';

NumWorkers = 4; % input the maximum number of workers for parallel processing

%%%%%%%%%%%%%% DEFINE PARAMETERS FOR VARIOUS THINGS %%%%%%%%%%%%%%%%%%%%%%%
colour = jet;
% Store all user-specified parameters in a ParamStruct
ParamStruct = struct;
%   User set parameters for BaSDI:
ParamStruct.FramesBin = 2000;   %time window, frames, to average over
ParamStruct.PixelBin = 10;      % pixel size in nanometers for BaSDI binning
ParamStruct.Iterations = 5;     % Number of BaSDI iterations
%   User set parameters for Photo-blinking correction
ParamStruct.MaxLinkingDistance = 75;    % in units of nm
ParamStruct.MaxGapClosing = 2;      % max number of blinks in frames
%   User set parameters for Segmenting the nuclei:
ParamStruct.LowResPixelSize = 60; % size of pixels in nm for segmentation
ParamStruct.NormI = 70; % percentile based image normalization
ParamStruct.MinLocs = 20;   % only calculate the LocError if there was at least 20 localizations in order to accurately calculate the mean:
% parameters for Bayes Clustering:
ParamStruct.MaxFails = 1000;
ParamStruct.SquareSize = 3000; % size of square in nanometer
ParamStruct.minDist = 1500; % minimum distance between square centroids in nanometer
%%%%%%%%%%%%%%%%%%%%% BayesPALM CLUSTERING R SCRIPTS %%%%%%%%%%%%%%%%%%%%%%
%   you will need to keep all the R scripts in the folder next to where you
%   want to run everything
main_scripts_path = ['.', filesep, 'BayesPALM_clustering', filesep]; 
main_scripts_names = {'internal.R', 'postprocessing.R', 'run.R', 'readme_Mac.docx'};
config_path = ['.', filesep, 'BayesPALM_clustering', filesep, 'Example', filesep];
config_file_name = 'config.txt';
%%%%%%%%%%%%%%%%%%%% K-Ripley analysis in Python and R %%%%%%%%%%%%%%%%%%%%
SpatialStats_path = ['.', filesep, 'SpatialStatistics_ASH', filesep];
SpatialStats_names = {'HowToRunPythonR_RipleyCode.rtf', 'SegmentNucleusForSpatialAnalysis.py', 'ProcessROutput.py', 'RunRipleyInR.r', 'BayesPALM_clustering.Rproj'};


% run through each file one at a time:
DirFiles=dir([InputPath,'*.mat']);
MatFiles = ''; %for saving the actual file name of the MAT file
for iter = 1:length(DirFiles)
    MatFiles{iter} = DirFiles(iter).name(1:end-12);
end

% Store everything in a single Structured Array
DataStruct = struct;
for iter = 1:length(MatFiles)
    DataStruct(iter,1).MatName = MatFiles{iter};
    DataStruct(iter,1).MatPath = [InputPath, MatFiles{iter}, '_Tracked.mat'];
end
% temp cell arrays for parallel processing:
temp_cell_matrix = cell(length(MatFiles), 1); temp_cell_drift = cell(length(MatFiles), 1);
temp_cell_PALM_Locs = cell(length(MatFiles), 1); temp_cell_TrackedLocs = cell(length(MatFiles), 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% STEP 1 - BaSDI-based drift correction %%%%%%%%%%%%%%%%%%
% The purpose here is to identify how much the sample drifted during the
% PALM acquisition and then to correct for this drift. 
tic; disp('--------------------------------------------------------------');
disp('Beginning the drift-correction - this is going to take a while...');
parpool('local', min([NumWorkers, length(MatFiles)]));
parfor iter = 1:length(DataStruct)
    % Do everything in a Sub-Function. All you need here is:
    %   - name and dir of MAT-file to load
    %   - ParamStruct with the relevant parameters
    disp(['BaSDI-drift correcting PALM movie ', num2str(iter), ' of ', num2str(length(DataStruct)), ' total']);
    %[DataStruct(iter,1).BaSDI_matrix, DataStruct(iter,1).Drift] = IterativeBaSDI_DriftCorrect(ParamStruct, DataStruct(iter,1).MatPath);
    [BaSDI_matrix, Drift] = IterativeBaSDI_DriftCorrect(ParamStruct, DataStruct(iter,1).MatPath);
    % save temporarily to cell array to appease the parfor Gods:
    temp_cell_matrix{iter,1} = BaSDI_matrix;
    temp_cell_drift{iter,1} = Drift;
end

% collect from the parfor loop
for iter = 1:length(DataStruct)
    DataStruct(iter,1).BaSDI_matrix = temp_cell_matrix{iter,1};
    DataStruct(iter,1).Drift = temp_cell_drift{iter,1};
end
toc; disp('--------------------------------------------------------------');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% STEP 2 - Merge photo-blinkers %%%%%%%%%%%%%%%%%%%%%%%%%%
% The purpose of this step is to correct for photo-blinking. For example, a
% single molecule may appear in many adjacent frames, which would look like
% a cluster even though it is not. Therefore, we must merged all of these
% localizations into a single localization, which we do using
% NearestNeighbour tracking subject to a max distance and max number of
% frame gaps. 
tic; disp('--------------------------------------------------------------');
disp('Beginning the photoblinking correction - this is also going to take a while...');
parfor iter = 1:length(DataStruct)
    % Do everything in a Sub-Function. All you need here is:
    %   - BaSDI drift-corrected matrix from above
    %   - ParamStruct with the relevant parameters
    disp(['Merging multiple appearances of the same molecule for ', num2str(iter), ' of ', num2str(length(DataStruct)), ' total']);
    [PALM_Locs, TrackedLocs] = MergeBlinks(ParamStruct, DataStruct(iter,1).BaSDI_matrix);
    % save temporarily to cell array to appease the parfor Gods:
    temp_cell_PALM_Locs{iter,1} = PALM_Locs;
    temp_cell_TrackedLocs{iter,1} = TrackedLocs;
end
% collect from the parfor loop
for iter = 1:length(DataStruct)
    DataStruct(iter,1).PALM_Locs = temp_cell_PALM_Locs{iter,1};
    DataStruct(iter,1).TrackedLocs = temp_cell_TrackedLocs{iter,1};
end
p = gcp; delete(p); % shut down the parallel pool
toc; disp('--------------------------------------------------------------');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% clear temp cell arrays:
clear temp_cell_matrix temp_cell_drift temp_cell_PALM_Locs temp_cell_TrackedLocs

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% STEP 3 - SEGMENT NUCLEI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for MatIter = 1:length(DataStruct)
    disp(['Segmenting nuclei in PALM movie ', num2str(MatIter), ' of ', num2str(length(DataStruct)), ' total movies.']);
    tic;
    curr_PALM_Locs = DataStruct(MatIter,1).PALM_Locs;
    curr_TrackedLocs = DataStruct(MatIter,1).TrackedLocs;
    % generate a low-res PALM image for polygon-mediated segmentation:
    Pixel_PALM_Locs = round( (curr_PALM_Locs(:,1:2) + ParamStruct.LowResPixelSize/2)/ParamStruct.LowResPixelSize);
    % max a rectangular image:
    LowResImg = zeros(max(Pixel_PALM_Locs(:,2)), max(Pixel_PALM_Locs(:,1)));
    % fill in the image:
    for LocIter = 1:size(Pixel_PALM_Locs,1)
        LowResImg(Pixel_PALM_Locs(LocIter,2), Pixel_PALM_Locs(LocIter,1)) = 1 + LowResImg(Pixel_PALM_Locs(LocIter,2), Pixel_PALM_Locs(LocIter,1));
    end
    
    % smooth the image a bit and normalize the intensity:
    MaxI = prctile(LowResImg(:), ParamStruct.NormI);
    LowResImgNorm = LowResImg ./ MaxI;
    LowResImgNorm((LowResImgNorm>1)) = 1;
    %%% GAUSSIAN SMOOTHING
    smooth_filter = fspecial('gaussian', [6 6], 2);
    LowResImgBlur = imfilter(LowResImgNorm, smooth_filter);
    
    %%%%%%%%%%%%%%%%%%% NUCLEAR SEGMENTATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    FinishedSegmentingNuclei = 'No'; NucleiCount = 0;
    % continue until user says they are finished. Store segmentation info
    % in a cell array
    SegmentedPolygonROIs = cell(3,1);
    while strcmp(FinishedSegmentingNuclei, 'No')
        NucleiCount = NucleiCount + 1;
        disp(['Segmenting nucleus #', num2str(NucleiCount)]);
        % segment the current nucleus
        SegmentedNucleus = roipoly(LowResImgBlur);
        % save to the cell array:
        % are there more to segment?
        SegmentedPolygonROIs{NucleiCount,1} = SegmentedNucleus;
        FinishedSegmentingNuclei = questdlg('are you finished? Answer YES to finish or NO to continue');
    end
    disp('OK, finished the segmentation. Just a bit more processing...');
    
    % collect the segmented nuclei:
    SegmentedPolygonROIs = SegmentedPolygonROIs(1:NucleiCount,1);
    % loop over each nucleus and collect the PALM localizations:
    PALM_Nuclei = cell(NucleiCount,1);
    Nuclei_TrackedLocs = cell(NucleiCount,1);
    for NucleiIter = 1:NucleiCount
        % collect localizations in the segmented nucleus
        curr_Nucleus = zeros(10^5,3);
        curr_Nucleus_TrackedLocs = cell(10^5,1); 
        curr_Segmentation = SegmentedPolygonROIs{NucleiIter,1};
        LocCount = 0;
        for LocIter = 1:size(curr_PALM_Locs,1)
            % see if the Loc was inside the segment nucleus
            if curr_Segmentation(Pixel_PALM_Locs(LocIter,2), Pixel_PALM_Locs(LocIter,1)) == 1
                LocCount = LocCount + 1; % increment the counter
                curr_Nucleus(LocCount,:) = curr_PALM_Locs(LocIter,:);
                % since the trajectories and PALM-Locs match, you can also
                % save the TrackedLocs trajectory:
                curr_Nucleus_TrackedLocs{LocCount} = curr_TrackedLocs{LocIter};
            end
        end
        curr_Nucleus = curr_Nucleus(1:LocCount,:); % remove extra zeros
        curr_Nucleus_TrackedLocs = curr_Nucleus_TrackedLocs(1:LocCount,1); % remove extra elements
        % save to cell array:
        PALM_Nuclei{NucleiIter,1} = curr_Nucleus;
        Nuclei_TrackedLocs{NucleiIter,1} = curr_Nucleus_TrackedLocs;
    end
    
    % save to DataStruct:
    DataStruct(MatIter,1).SegmentedPolygonROIs = SegmentedPolygonROIs;
    DataStruct(MatIter,1).PALM_Nuclei = PALM_Nuclei;
    DataStruct(MatIter,1).Nuclei_TrackedLocs = Nuclei_TrackedLocs;
    toc;
    

end
clear curr_Nucleus curr_PALM_Locs curr_Segmentation MaxI LowResImgNorm LowResImgBlur FinishedSegmentingNuclei NucleiCount
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for MatIter = 1:length(DataStruct)
    disp('--------------------------------------------');
    disp(['Processing nuclei in PALM movie ', num2str(MatIter), ' of ', num2str(length(DataStruct)), ' total movies.']);
    
    % loop over each nuclei
    for NucIter = 1:length(DataStruct(MatIter,1).PALM_Nuclei)
        close all;
        tic; disp(['Processing nucleus ', num2str(NucIter), ' of ', num2str(length(DataStruct(MatIter,1).PALM_Nuclei)), ' total nuclei']);
        % clear out previous VARs:
        clear nuclear_PALM_locs TrackedLocs SegmentedPolygonROI DriftStruct MatName
        
        % take out the current localizations:
        curr_PALM_Locs = DataStruct(MatIter,1).PALM_Nuclei{NucIter};
        TrackedLocs = DataStruct(MatIter,1).Nuclei_TrackedLocs{NucIter};
        SegmentedPolygonROI = DataStruct(MatIter,1).SegmentedPolygonROIs{NucIter};
        DriftStruct = DataStruct(MatIter,1).Drift;
        MatName = DataStruct(MatIter,1).MatName;
        
        %%%%%%%%%%%%%%%%%%%% CALCULATION 1 and 2 %%%%%%%%%%%%%%%%%%%%%%%%%%
        % Calculate the number of localizations per frame
        % Calculate the Localization Error
        
        % NB: You need to calculate the localization error for your own
        % sake, but also because the Bayesian Cluster Identification
        % Algorithm needs the information. So make a new matrix:
        nuclear_PALM_locs = zeros(size(curr_PALM_Locs,1), 4);
        %   col 1: X, col 2: Y, col 3: Frame, col 4: Loc Error
        nuclear_PALM_locs(:,1:3) = curr_PALM_Locs;
        
        % first need to get max number of frames:
        MaxFrame = round(max(curr_PALM_Locs(:,3))/1000) * 1000;
        LocsPerFrame = zeros(MaxFrame,1); % for storing Locs
        
        % Also need matrix for storing Loc Errors:
        XY_LocErrors = zeros(10^5,2); % col 1: X, col 2: Y
        ErrorCount = 1;
        
        % Also need to store particle lifetimes (number of frames)
        LocLifetime = zeros(10^5,1);
        LocCount = 0;
        
        % loop over tracked Locs Now:
        for TrackIter = 1:length(TrackedLocs)
            curr_traj = TrackedLocs{TrackIter};
            % loop over each trajectory
            for iter = 1:size(curr_traj,1)
                LocsPerFrame( curr_traj(iter,3) ) = LocsPerFrame( curr_traj(iter,3) ) + 1;
            end
            
            TrajLength = size(curr_traj,1);
            % next see if Trajectory is long enough to reliably calculate
            % the localization error:
            if TrajLength >= ParamStruct.MinLocs
                % OK, it's long enough                
                X_mean = mean(curr_traj(:,1));
                Y_mean = mean(curr_traj(:,2));
                % now get the errors:
                X_errors = curr_traj(:,1) - X_mean;
                Y_errors = curr_traj(:,2) - Y_mean;
                % now store the errors:
                XY_LocErrors(ErrorCount:(ErrorCount+TrajLength-1), 1) = X_errors; % store the X errors
                XY_LocErrors(ErrorCount:(ErrorCount+TrajLength-1), 2) = Y_errors; % store the Y errors
                % update the counter:
                ErrorCount = ErrorCount + TrajLength;
                
                % store the Loc-specific localization error:
                nuclear_PALM_locs(TrackIter, 4) = mean(abs(vertcat(X_errors, Y_errors)));
            end
            
            % Finally store trajectory length:
            LocCount = LocCount + 1; % increment
            LocLifetime(LocCount,1) = TrajLength;
            
        end
        % make sure you don't have left-over zeros in the matrix
        XY_LocErrors = XY_LocErrors(1:(ErrorCount-1),:);
        LocLifetime = LocLifetime(1:LocCount);
        
        % fill in the remaining average localization errors.
        Idx = find(nuclear_PALM_locs(:,4) == 0);
        AvgLocError = mean(abs(vertcat(XY_LocErrors(:,1), XY_LocErrors(:,2))));
        nuclear_PALM_locs(Idx,4) = AvgLocError;
        
        % Proceed to finding squares for Bayesian Clustering:
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%% GENERATE SQUARES FOR BAYESIAN CLUSTERING %%%%%%%%%%%%%%%%
        %   APPROACH:
        %   Take a fairly brute force approach.
        %   This does not bias the result, but it is embarrasingly
        %   unsophisticated from a programming point-of-view.
        %   Algorithm:
        %               1. generate random x,y centroid
        %               2. draw user-defined square around centroid
        %               3. check to see if it is all within segmented nucleus
        %               4. yes? then save; No? then try again.
        %               5. repeat until enough squares have been found

        %   Store squares as a Mx4 matrix:
        %   each row is a different square
        %   Col 1: Min x; Col 2: Max x;
        %   Col 3: Min y; Col 4: Max y;
        %   save in units
        Square_parameters = zeros(1, 4);
        SquaresFound = 0;
        CurrFails = 0; 
        minDistPixels = ParamStruct.minDist/ParamStruct.LowResPixelSize;
        disp('searching for squares...'); 
        % first get the min and max Row/Col vals:
        [Rows, Cols] = find(SegmentedPolygonROI==1);
        MinMaxRowsCols = [min(Rows) max(Rows) min(Cols) max(Cols)];
        %   Now find some squares:
        while CurrFails < ParamStruct.MaxFails
            %Generate an x,y centroid guess in units of pixels:
            guess_x = MinMaxRowsCols(3) + rand * (MinMaxRowsCols(4)-MinMaxRowsCols(3));
            guess_y = MinMaxRowsCols(1) + rand * (MinMaxRowsCols(2)-MinMaxRowsCols(1));
            
            % Use units of pixel for now:
            extend_in_pixels = (0.5 * ParamStruct.SquareSize / ParamStruct.LowResPixelSize);
            min_x = guess_x - extend_in_pixels;
            max_x = guess_x + extend_in_pixels;
            min_y = guess_y - extend_in_pixels;
            max_y = guess_y + extend_in_pixels;
            % make sure that you are not close to the edge of the image:
            if min_x - ceil(extend_in_pixels) > 0 && max_x + ceil(extend_in_pixels) < MinMaxRowsCols(4)
                if min_y - ceil(extend_in_pixels) > 0 && max_y + ceil(extend_in_pixels) < MinMaxRowsCols(2)
                    % Take a slice of the segmented nucleus and make sure no values are
                    % equal to zero, which indicates they are outside the nucleus.
                    SliceOfImage = SegmentedPolygonROI(round(min_y):round(max_y), round(min_x):round(max_x));

                    %Now check to see if the slice is all nuclear:
                    if min(SliceOfImage(:)) == 1
                        % OK, this square is all nuclear.
                        % But also check that it is not too close to an
                        % existing square. Calculate the distances to all
                        % already chosen squares:
                        NotTooClose = 0;
                        if SquaresFound == 0
                            %OK, this is the first one, so just save it:
                            NotTooClose = 1;
                        else
                            %OK, you already have at least one square. So
                            %calculate the distances:
                            all_dists = zeros(1, SquaresFound);
                            for i=1:length(all_dists)
                                all_dists(1,i) = pdist([guess_x, guess_y; mean(Square_parameters(i,1:2))/ParamStruct.LowResPixelSize, mean(Square_parameters(i,3:4))/ParamStruct.LowResPixelSize]);
                            end
                            if min(all_dists) > minDistPixels
                                NotTooClose = 1;
                            end
                        end    

                        %OK, not too close, so save it:
                        if NotTooClose == 1
                            SquaresFound = SquaresFound + 1;
                            %save min/max values and convert to nanometer
                            Square_parameters(SquaresFound,:) = ParamStruct.LowResPixelSize .*[min_x, max_x, min_y, max_y];
                            disp(['found a new square. We now have ', num2str(SquaresFound), ' squares']);
                        else
                            CurrFails = CurrFails + 1;
                        end
                    else
                        CurrFails = CurrFails + 1;
                    end   
                else
                    CurrFails = CurrFails + 1;
                end
            else
                CurrFails = CurrFails + 1;
            end
            
            
        end
        disp('OK, done with finding squares for Bayesian clustering');
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%% SAVE THE WORKSPACE %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        disp('make a new directory and save the MAT-file');
        % Generate a name for the directory:
        DirName = [MatName, 'Nucleus', num2str(NucIter)];
        % make the new directory:
        mkdir([OutputPath, DirName]);
        % now save the relevant variables:
        save([OutputPath, DirName, filesep, DirName, '.mat'], 'nuclear_PALM_locs', 'TrackedLocs', 'SegmentedPolygonROI', 'DriftStruct', ...
                    'MatName', 'Square_parameters', 'ParamStruct', 'XY_LocErrors', 'LocsPerFrame', 'LocLifetime', 'AvgLocError');
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%% PLOT THE KEY INFO  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        figure1 = figure('position',[100 100 1000 1400]); %[x y width height]
        
        %%%% PLOT NUCLEUS WITH SQUARES
        subplot(4,3,[1,2,4,5]);
        hold on;
        plot(curr_PALM_Locs(:,1), curr_PALM_Locs(:,2), 'ko', 'MarkerSize', 1);
        %   now overlay all of the squares:
        for i=1:size(Square_parameters,1)
            min_x = Square_parameters(i,1);
            max_x = Square_parameters(i,2);
            min_y = Square_parameters(i,3);
            max_y = Square_parameters(i,4);
            colour_element = colour(round(i/size(Square_parameters,1)*size(colour,1)),:);
            plot([min_x min_x], [min_y max_y], '-', 'LineWidth', 3, 'Color', colour_element);
            plot([min_x max_x], [min_y min_y], '-', 'LineWidth', 3,'Color', colour_element);
            plot([min_x max_x], [max_y max_y], '-', 'LineWidth', 3,'Color', colour_element);
            plot([max_x max_x], [min_y max_y], '-', 'LineWidth', 3,'Color', colour_element);
        end
        axis([0.95*min(curr_PALM_Locs(:,1)) 1.05*max(curr_PALM_Locs(:,1)) 0.95*min(curr_PALM_Locs(:,2)) 1.01*max(curr_PALM_Locs(:,2))]);
        xlabel('X (nm)', 'FontSize',9, 'FontName', 'Helvetica');
        ylabel('Y (nm)', 'FontSize',9, 'FontName', 'Helvetica');
        title({DirName; ['PALM reconstruction: squares for analysis overlaid; Nuclear size = ' num2str(length(Rows)*ParamStruct.LowResPixelSize/1000), ' um2'];...
            ['Total Locs: ', num2str(size(curr_PALM_Locs,1))];}, 'FontSize',8, 'FontName', 'Helvetica', 'FontWeight', 'Normal', 'Interpreter', 'none');
        set(gca,'YDir','Reverse')
        hold off;
        
        %%%% PLOT X AND Y LOCALIZATION ERRORs
        subplot(4,3,3);
        hold on;
        histogram(XY_LocErrors(:,1), 2*ParamStruct.MaxLinkingDistance+2, 'FaceColor', [0/255, 153/255, 204/255]);
        xlim([-ParamStruct.MaxLinkingDistance-1, +ParamStruct.MaxLinkingDistance+1]);
        xlabel('Loc Error (nm)', 'FontSize',9, 'FontName', 'Helvetica');
        ylabel('frequency', 'FontSize',9, 'FontName', 'Helvetica');
        title(['<\sigma_X> = ', num2str(mean(abs(XY_LocErrors(:,1)))), ' nm'], 'FontSize',10, 'FontName', 'Helvetica', 'FontWeight', 'Normal');
        hold off;
        
        subplot(4,3,6);
        hold on;
        histogram(XY_LocErrors(:,2), 2*ParamStruct.MaxLinkingDistance+2, 'FaceColor', [237/255, 28/255, 36/255]);
        xlim([-ParamStruct.MaxLinkingDistance-1, +ParamStruct.MaxLinkingDistance+1]);
        xlabel('Loc Error (nm)', 'FontSize',9, 'FontName', 'Helvetica');
        ylabel('frequency', 'FontSize',9, 'FontName', 'Helvetica');
        title(['<\sigma_Y> = ', num2str(mean(abs(XY_LocErrors(:,2)))), ' nm'], 'FontSize',10, 'FontName', 'Helvetica', 'FontWeight', 'Normal');
        hold off;
        
        %%%%% PLOT THE TOTAL DRIFT
        subplot(4,3,7);
        hold on;
        plot(1:MaxFrame, DriftStruct.TotalDrift(1:MaxFrame,1), '-', 'LineWidth', 2, 'Color', [0/255, 153/255, 204/255]);
        plot(1:MaxFrame, DriftStruct.TotalDrift(1:MaxFrame,2), '-', 'LineWidth', 2, 'Color', [237/255, 28/255, 36/255]);
        xlim([0 MaxFrame]);
        xlabel('frame number', 'FontSize',9, 'FontName', 'Helvetica');
        ylabel('drift (nm)', 'FontSize',9, 'FontName', 'Helvetica');
        title('Total BaSDI-inferred drift', 'FontSize',10, 'FontName', 'Helvetica', 'FontWeight', 'Normal');
        legend('X','Y', 'Location', 'SouthWest');
        hold off;
        
        %%%%% PLOT THE X-DRIFT IN EACH ITERATION:
        subplot(4,3,8);
        % generate a color map for iterative plots:
        LegendLabels = {''};
        hold on;
        for iter = 1:ParamStruct.Iterations
            curr_drift = DriftStruct.IterDrift_XY_nm{iter};
            colour_element = colour(round(iter/ParamStruct.Iterations*size(colour,1)),:);
            plot(1:MaxFrame, curr_drift(1:MaxFrame,1), '-', 'LineWidth', 1, 'Color', colour_element);
            LegendLabels{iter} = ['iteration ', num2str(iter)];
        end
        xlim([0 MaxFrame]);
        xlabel('frame number', 'FontSize',9, 'FontName', 'Helvetica');
        ylabel('x-drift (nm)', 'FontSize',9, 'FontName', 'Helvetica');
        title('Total BaSDI-inferred drift: X', 'FontSize',10, 'FontName', 'Helvetica', 'FontWeight', 'Normal');
        legend(LegendLabels, 'Location', 'SouthWest');
        hold off;
        
        %%%%% PLOT THE Y-DRIFT IN EACH ITERATION:
        subplot(4,3,9);
        % generate a color map for iterative plots:
        LegendLabels = {''};
        hold on;
        for iter = 1:ParamStruct.Iterations
            curr_drift = DriftStruct.IterDrift_XY_nm{iter};
            colour_element = colour(round(iter/ParamStruct.Iterations*size(colour,1)),:);
            plot(1:MaxFrame, curr_drift(1:MaxFrame,2), '-', 'LineWidth', 1, 'Color', colour_element);
            LegendLabels{iter} = ['iteration ', num2str(iter)];
        end
        xlim([0 MaxFrame]);
        xlabel('frame number', 'FontSize',9, 'FontName', 'Helvetica');
        ylabel('Y-drift (nm)', 'FontSize',9, 'FontName', 'Helvetica');
        title('Total BaSDI-inferred drift: Y', 'FontSize',10, 'FontName', 'Helvetica', 'FontWeight', 'Normal');
        legend(LegendLabels, 'Location', 'SouthWest');
        hold off;
        
        
        %%%%% PLOT LOCS PER FRAME
        subplot(4,3,10); hold on;
        plot(1:MaxFrame, LocsPerFrame, 'ro', 'MarkerFaceColor', [237/255, 28/255, 36/255], 'MarkerSize', 4);
        plot(1:MaxFrame, smooth(LocsPerFrame, 100), 'k-', 'LineWidth', 2);
        xlim([0 MaxFrame]);
        xlabel('frame number', 'FontSize',9, 'FontName', 'Helvetica');
        ylabel('Detections per frame', 'FontSize',9, 'FontName', 'Helvetica');
        SizeOfNucleus = length(Rows)*ParamStruct.LowResPixelSize/1000;
        LocsPerSquareUmPerFrame = mean(LocsPerFrame) / SizeOfNucleus;
        title(['Mean: ', num2str(round(mean(LocsPerFrame)*10)/10), ' Loc/Frame; <Locs per \mum^2> =', num2str(LocsPerSquareUmPerFrame)], 'FontSize',10, 'FontName', 'Helvetica', 'FontWeight', 'Normal');
        hold off;
        
        %%%%% PLOT LIFETIME DISTRIBUTION
        % make a CDF:
        SurvivalProb = 1-histcounts(LocLifetime, 'Normalization', 'cdf');
        X_frames = min([200 length(SurvivalProb)]);
        subplot(4,3,11); hold on;
        plot(1:X_frames, SurvivalProb(1:X_frames), 'ko', 'MarkerSize', 4, 'MarkerFaceColor', [237/255, 28/255, 36/255]);
        set(gca, 'YScale', 'log');
        xlabel('frame number', 'FontSize',9, 'FontName', 'Helvetica');
        ylabel('1-CDF (fluorophore lifetime)', 'FontSize',9, 'FontName', 'Helvetica');
        title(['Mean lifetime: ', num2str(round(mean(LocLifetime)*10)/10), ' frames'], 'FontSize',10, 'FontName', 'Helvetica', 'FontWeight', 'Normal');
        hold off;
        
        %%%%% SAVE A PDF FIGURE:
        set(figure1,'Units','Inches');
        pos = get(figure1,'Position');
        set(figure1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
        %print(figure1,[OutputPath, DirName, filesep, 'SummaryPlotsOfPALM.pdf'],'-dpdf','-r0');
        print(figure1,[OutputPath, DirName, filesep, 'SummaryPlotsOfPALM.png'],'-dpng','-r300');
        
        disp('finished saving the summary plot');
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%% WRITE DATA IN FORMAT FOR BAYESIAN CLUSTERING %%%%%%%%%%%%%%

        %   The Bayes Cluster code uses a pretty annoying format and must run
        %   in R, but we have to just deal with it. So write data in their
        %   annoying data format. 
        %   For each square then, you will have to make a separate data.txt
        %   file and put it in a separate folder
        disp('saving folders for each square');
        % need to save a bunch of sub-folders, so give the relevant path a
        % new name:
        EffInputPath = [OutputPath, DirName, filesep];
        %   Make a directory for Bayes Clustering:
        mkdir(EffInputPath, 'BayesPALM_clustering');
        %Copy the relevant R files to this directory:
        for iter=1:length(main_scripts_names)
            copyfile([main_scripts_path, main_scripts_names{iter}], [EffInputPath, '/BayesPALM_clustering/', main_scripts_names{iter}]);
        end
        % Now make a subfolder, 'DataFolder', where you will put all of the data
        % and parameters:
        mkdir(EffInputPath, 'BayesPALM_clustering/DataFolder');
        copyfile([config_path, config_file_name], [EffInputPath, 'BayesPALM_clustering/DataFolder/', config_file_name]);
        
        %%% Make a folder with a numeric name and put the data in it
       for iter=1:size(Square_parameters,1)
           %make a folder entitled 1,2,3,...,n
           mkdir([EffInputPath, 'BayesPALM_clustering/DataFolder/', num2str(iter)]);

           %re-scale the data so all x,y values are between 0 and 3000 nm and
           %then save it in the csv format.
           %remember to include the header" x,y,sd
           min_x = Square_parameters(iter,1);
           max_x = Square_parameters(iter,2);
           min_y = Square_parameters(iter,3);
           max_y = Square_parameters(iter,4);

           %Subtract the min_x, min_y from all PALM locs:
           temp_PALM_locs = nuclear_PALM_locs;
           temp_PALM_locs(:,1) = temp_PALM_locs(:,1) - min_x;
           temp_PALM_locs(:,2) = temp_PALM_locs(:,2) - min_y;
           %Find the elements whose x,y values fall within the square:
           Idx_x = find(temp_PALM_locs(:,1) > 0 & temp_PALM_locs(:,1) < ParamStruct.SquareSize);
           Idx_y = find(temp_PALM_locs(:,2) > 0 & temp_PALM_locs(:,2) < ParamStruct.SquareSize);
           %Find the elements where both the x- and y-value is within the
           %thresholds:
           Idx_both = intersect(Idx_x, Idx_y);

           % Now write everything to a CSV file:
           Data_to_write = temp_PALM_locs(Idx_both,:);
           %Convert data to a table with header:
           x = Data_to_write(:,1);
           y = Data_to_write(:,2);
           sd = Data_to_write(:,4);
           curr_table = table(x,y,sd);
           writetable(curr_table, [EffInputPath, 'BayesPALM_clustering/DataFolder/', num2str(iter), '/data.txt']);


           clear temp_PALM_locs curr_table
       end
       
       
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%% WRITE DATA IN FORMAT FOR K-RIPLEY ANALYSIS %%%%%%%%%%%%

        %   The K-Ripley code runs in Python and R, so save the relevant
        %   scripts and files to each subfolder
        disp('saving scripts for K-Ripley analysis');
        % need to save a bunch of sub-folders, so give the relevant path a
        % new name:
        EffInputPath = [OutputPath, DirName, filesep];
        %   Make a directory for Bayes Clustering:
        mkdir(EffInputPath, 'KRipleyScripts');
        %Copy the relevant R files to this directory:
        for iter=1:length(main_scripts_names)
            copyfile([SpatialStats_path, SpatialStats_names{iter}], [EffInputPath, '/KRipleyScripts/', SpatialStats_names{iter}]);
        end

        
        toc;
                
    end
    
    
    
    
end

